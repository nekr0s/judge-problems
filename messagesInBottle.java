import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static String secretCodeNumbers;
	public static List<String> list;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		secretCodeNumbers = in.nextLine();
		list = new ArrayList<>();

		String[] cipher = in.nextLine().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		HashMap<String, String> map = new HashMap<>();
		for (int i = 0; i < cipher.length; i += 2) {
			map.put(cipher[i], cipher[i + 1]);
		}

		solve(0, new StringBuilder(), map);

		System.out.println(count);
		list.sort(null);
		for (String s : list) {
			System.out.println(s);
		}
	}

	public static int count = 0;

	private static void solve(int index, StringBuilder sb, HashMap<String, String> map) {

		if (index == secretCodeNumbers.length()) {
			count++;
			list.add(sb.toString());
			return;
		}

		// use
		for (String key : map.keySet()) {
			String smallChecker = map.get(key);
			String bigChecker = secretCodeNumbers.substring(index);
			if (bigChecker.startsWith(smallChecker)) {
				sb.append(key);
				solve(index + map.get(key).length(), sb, map);
				sb.deleteCharAt(sb.length() - 1);
			}
		}

	}
}