import java.util.Scanner;

public class Main {

	public static int bestScore = Integer.MIN_VALUE;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		int x = input.charAt(0) - '0';
		int y = input.charAt(2) - '0';

		String input2 = in.nextLine();
		int row = input2.charAt(0) - '0';
		int col = input2.charAt(2) - '0';

		char[][] matrix = new char[row][col];

		for (int i = 0; i < matrix.length; i++) {
			String[] line = in.nextLine().split(" ");
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = line[j].charAt(0);
			}
		}

		boolean[][] used = new boolean[row][col];
		// used[x][y] = true;
		dfs(x, y, matrix, used, 0);
		System.out.println(bestScore);

	}

	private static void dfs(int x, int y, char[][] matrix, boolean[][] used, int score) {
		int[] dRows = { -matrix[x][y] + '0', matrix[x][y] - '0', 0, 0 };
		int[] dCols = { 0, 0, -matrix[x][y] + '0', matrix[x][y] - '0' };

		// score += matrix[x][y] - '0';

		for (int dir = 0; dir < dRows.length; dir++) {
			int nextRow = x + dRows[dir];
			int nextCol = y + dCols[dir];

			if (nextRow < 0 || nextRow >= matrix.length || nextCol < 0 || nextCol >= matrix[0].length) {
				continue;
			}

			if (matrix[nextRow][nextCol] == '#') {
				continue;
			}

			if (used[x][y] == true) {
				break;
			} else {
				used[x][y] = true;
			}

			// used[nextRow][nextCol] = true;
			score += matrix[x][y] - '0';
			dfs(nextRow, nextCol, matrix, used, score);
			score -= matrix[x][y] - '0';
			used[x][y] = false;

		}
		// score += matrix[x][y] - '0';
		bestScore = Math.max(score, bestScore);

	}
}