import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Main {

	public static class Unit implements Comparable<Unit> {

		public String name;
		public String type;
		public int attack;

		public Unit(String name, String type, int attack) {
			this.name = name;
			this.type = type;
			this.attack = attack;
		}

		@Override
		public String toString() {
			return this.name + "[" + this.type + "](" + this.attack + ")";
		}

		@Override
		public int compareTo(Unit o) {
			if (this.attack - o.attack != 0) {
				return o.attack - this.attack;
			}

			return this.name.compareTo(o.name);
		}

	}

	private static Map<String, Unit> unitsMap = new HashMap<String, Unit>();
	private static Map<String, TreeSet<Unit>> byType = new HashMap<String, TreeSet<Unit>>();
	private static TreeSet<Unit> orderedUnits = new TreeSet<>();
	private static StringBuilder output = new StringBuilder();

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		// String[] input = null;
		while (true) {
			String nextLine = in.nextLine();
			String[] input = nextLine.split(" ");
			switch (input[0].toLowerCase()) {
			case "end":
				System.out.println(output.toString());
				return;
			case "add":
				Unit unitToAdd = new Unit(input[1], input[2], Integer.parseInt(input[3]));

				if (unitsMap.containsKey(unitToAdd.name)) {
					output.append("FAIL: " + unitToAdd.name + " already exists!\n");
					break;
				}

				unitsMap.put(unitToAdd.name, unitToAdd);

				if (!byType.containsKey(unitToAdd.type)) {
					byType.put(unitToAdd.type, new TreeSet<Unit>());
				}

				byType.get(unitToAdd.type).add(unitToAdd);
				orderedUnits.add(unitToAdd);

				output.append("SUCCESS: " + unitToAdd.name + " added!\n");
				break;
			case "remove":

				String name = input[1];

				if (!unitsMap.containsKey(name)) {
					output.append("FAIL: " + name + " could not be found!\n");
					break;
				}

				Unit toRemove = unitsMap.get(name);
				unitsMap.remove(toRemove.name);

				byType.get(toRemove.type).remove(toRemove);
				orderedUnits.remove(toRemove);

				output.append("SUCCESS: " + name + " removed!\n");
				break;
			case "find":
				output.append("RESULT: ");
				if (byType.containsKey(input[1])) {
					output.append(byType.get(input[1]).stream().limit(10).map(x -> x.toString())
							.collect(Collectors.joining(", ")));
				}
				output.append("\n");
				break;
			case "power":
				getByPower(Integer.parseInt(input[1]));
				break;
			default:
				break;

			}
			// input = in.nextLine().split(" ");
		}
	}

	private static void getByPower(int power) {
		output.append("RESULT: ");
		output.append(orderedUnits.stream().limit(power).map(x -> x.toString()).collect(Collectors.joining(", ")));
		output.append("\n");

	}
}