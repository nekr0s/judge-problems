import java.util.Scanner;

public class PathToOne {

	static int globalSteps = Integer.MAX_VALUE;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int steps = 0;

		pathToOne(n, steps);
		System.out.println(globalSteps);
	}

	static void pathToOne(int number, int currentSteps) {

		if (number == 1) {
			globalSteps = Math.min(globalSteps, currentSteps);
			return;
		}

		if (number % 2 == 0) {
			// is even
			pathToOne(number / 2, currentSteps + 1);
		} else {
			pathToOne(number - 1, currentSteps + 1);
			pathToOne(number + 1, currentSteps + 1);
		}

	}
}