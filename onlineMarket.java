import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Market {

	public static class Product implements Comparable<Product> {
		String name;
		Double price;
		String type;

		public Product(String name, Double price, String type) {
			this.name = name;
			this.price = price;
			this.type = type;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(this.name).append("(")
					.append(new BigDecimal(String.valueOf(this.price)).stripTrailingZeros().toPlainString())
					.append(")");
			return sb.toString();
		}

		@Override
		public int compareTo(Product other) {
			if (Double.compare(this.price, other.price) == 0 && !this.name.equals(other.name)) {
				return this.name.compareTo(other.name);
			} else if (this.price == other.price && this.name.equals(other.name)) {
				return this.type.compareTo(other.type);
			}
			return Double.compare(this.price, other.price);
		}

	}

	public static Map<String, TreeSet<Product>> productsByType = new HashMap<>();
	public static Map<String, Product> productsByName = new HashMap<>();
	public static TreeSet<Product> productsByPrice = new TreeSet<>();
	public static StringBuilder output = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String[] command = in.readLine().split(" ");

		while (!command[0].equals("end")) {
			switch (command[0]) {
			case "add":
				Product newProduct = new Product(command[1], Double.parseDouble(command[2]), command[3]);
				handleAddProduct(newProduct);
				break;
			case "filter":
				if (command[2].equals("type")) {
					handleFilterByType(command[3]);
					break;
				} else if (command[2].equals("price")) {
					if (command.length > 5) {
						Double from = Double.parseDouble(command[4]);
						Double to = Double.parseDouble(command[6]);
						handleFilterByPriceFromTo(from, to);
						break;
					} else {
						if (command[3].equals("from")) {
							Double from = Double.parseDouble(command[4]);
							handleFilterByPriceFrom(from);
							break;
						} else {
							Double to = Double.parseDouble(command[4]);
							handleFilterByPriceTo(to);
							break;
						}
					}
				}
				break;
			default:
				break;

			}

			command = in.readLine().split(" ");
		}

		System.out.println(output.toString());

	}

	private static void handleFilterByPriceTo(Double to) {
		StringBuilder sb = new StringBuilder();
		List<Product> result = productsByPrice.stream().filter(p -> p.price <= to).limit(10)
				.collect(Collectors.toList());

		if (result.isEmpty()) {
			output.append("Ok: ").append("\n");
			return;
		}

		for (Product p : result) {
			sb.append(p).append(", ");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		output.append("Ok: ").append(sb).append("\n");
	}

	private static void handleFilterByPriceFrom(Double from) {
		StringBuilder sb = new StringBuilder();
		List<Product> result = productsByPrice.stream().filter(p -> p.price >= from).limit(10)
				.collect(Collectors.toList());

		if (result.isEmpty()) {
			output.append("Ok: ").append("\n");
			return;
		}

		for (Product p : result) {
			sb.append(p).append(", ");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		output.append("Ok: ").append(sb).append("\n");

	}

	private static void handleFilterByPriceFromTo(Double from, Double to) {
		StringBuilder sb = new StringBuilder();
		List<Product> result = productsByPrice.stream().filter(p -> p.price >= from && p.price <= to).limit(10)
				.collect(Collectors.toList());

		if (result.isEmpty()) {
			output.append("Ok: ").append("\n");
			return;
		}

		for (Product p : result) {
			sb.append(p).append(", ");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		output.append("Ok: ").append(sb).append("\n");
	}

	private static void handleFilterByType(String typeToFind) {
		if (!productsByType.containsKey(typeToFind)) {
			output.append("Error: Type ").append(typeToFind).append(" does not exists").append("\n");
			return;
		}

		// top 10
		StringBuilder sb = new StringBuilder();
		productsByType.get(typeToFind).stream().limit(10).forEach(product -> sb.append(product).append(", "));
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		output.append("Ok: ").append(sb).append("\n");
	}

	private static void handleAddProduct(Product newProduct) {
		if (productsByName.containsKey(newProduct.name)) {
			output.append("Error: Product ").append(newProduct.name).append(" already exists").append("\n");
			return;
		}
		if (!productsByType.containsKey(newProduct.type)) {
			productsByType.put(newProduct.type, new TreeSet<>());
		}
		if (!productsByPrice.contains(newProduct)) {
			productsByPrice.add(newProduct);
		}

		productsByName.put(newProduct.name, newProduct);
		productsByType.get(newProduct.type).add(newProduct);
		output.append("Ok: Product ").append(newProduct.name).append(" added successfully").append("\n");
		return;

	}
}