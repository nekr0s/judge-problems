import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String smallString = in.nextLine();
		String bigString = in.nextLine();

		char[] smallArray = smallString.toCharArray();
		char[] bigArray = bigString.toCharArray();

		boolean[] checker = new boolean[smallString.length()];

		int starter = 0;
		for (int i = 0; i < smallArray.length; i++) {

			for (int j = starter; j < bigArray.length; j++) {
				if (smallArray[i] == bigArray[j]) {
					checker[i] = true;
					starter = j;
					break;
				}
			}
		}

		System.out.println(areAllTrue(checker));

	}

	public static boolean areAllTrue(boolean[] array) {
		for (boolean b : array)
			if (!b)
				return false;
		return true;
	}

}