import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static int maxMoney;
	public static int bestNode;
	public static int[] money;

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(in.readLine());
		String[] coinString = in.readLine().split(" "); // coins
		money = new int[n];
		for (int i = 0; i < n; i++) {
			money[i] = Integer.parseInt(coinString[i]);
		}

		Graph graph = new Graph(n);

		for (int i = 0; i < n - 1; i++) {
			String[] input = in.readLine().split(" ");
			graph.addEdge(Integer.valueOf(input[0]) - 1, Integer.valueOf(input[1]) - 1);
		}

		maxMoney = 0;
		bestNode = -1;
		graph.dfs(0, -1, 0);

		maxMoney = 0;
		graph.dfs(bestNode, -1, 0);
		System.out.println(maxMoney);

	}

	public static class Graph {
		List<List<Integer>> vertices;

		public Graph(int n) {
			vertices = new ArrayList<>();
			for (int i = 0; i < n; i++) {
				vertices.add(new ArrayList<>());
			}

		}

		public void dfs(int x, int prev, int tempMoney) {
			tempMoney += money[x];
			boolean hasNext = false;
			for (int i : vertices.get(x)) {
				if (i != prev) {
					hasNext = true;
					dfs(i, x, tempMoney);
				}
			}

			if (!hasNext) {
				if (tempMoney > maxMoney) {
					maxMoney = tempMoney;
					bestNode = x;
				}
			}

		}

		public void addEdge(int x, int y) {
			addDirectedEdge(x, y);
			addDirectedEdge(y, x);

		}

		private void addDirectedEdge(int from, int to) {
			vertices.get(from).add(to);

		}

	}
}