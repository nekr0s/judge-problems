import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class Main {

	public static class Player implements Comparable<Player> {
		String name;
		String type;
		int age;

		public Player(String name, String type, int age) {
			this.name = name;
			this.type = type;
			this.age = age;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(name).append("(").append(age).append(")");

			return sb.toString();
		}

		@Override
		public int compareTo(Player other) {
			if (this.name.equals(other.name)) {
				return Integer.compare(other.age, this.age);
			}
			return this.name.compareTo(other.name);
		}

	}

	public static Map<String, TreeSet<Player>> playersByType = new HashMap<>();
	public static List<Player> rankings = new ArrayList<>();
	public static StringBuilder output = new StringBuilder();

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String[] command = in.readLine().split(" ");
		while (!command[0].equals("end")) {

			switch (command[0]) {
			case "add":
				Player newPlayer = new Player(command[1], command[2], Integer.parseInt(command[3]));
				int positionToAdd = Integer.parseInt(command[4]) - 1;
				handleAddPlayer(newPlayer, positionToAdd);
				break;
			case "find":
				String typeToFind = command[1];
				handleFindPlayers(typeToFind);
				break;
			case "ranklist":
				int from = Integer.parseInt(command[1]) - 1;
				int to = Integer.parseInt(command[2]) - 1; // inclusive
				handleShowRanklist(from, to);
				break;
			default:
				break;

			}

			command = in.readLine().split(" ");
		}

		System.out.println(output);

	}

	private static void handleShowRanklist(int from, int to) {
		for (int i = from; i <= to; i++) {

			output.append(i + 1).append(". ").append(rankings.get(i).toString()).append("; ");

		}

		output.deleteCharAt(output.length() - 1);
		output.deleteCharAt(output.length() - 1);
		output.append("\n");

	}

	private static void handleFindPlayers(String typeToFind) {
		if (!playersByType.containsKey(typeToFind)) {
			output.append("Type ").append(typeToFind).append(": ").append("\n");
			return;
		}

		// top 5
		StringBuilder sb = new StringBuilder();
		playersByType.get(typeToFind).stream().limit(5).forEach(player -> sb.append(player).append("; "));

		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);

		output.append("Type ").append(typeToFind).append(": ").append(sb).append("\n");

	}

	private static void handleAddPlayer(Player newPlayer, int positionToAdd) {
		if (!playersByType.containsKey(newPlayer.type)) {
			playersByType.put(newPlayer.type, new TreeSet<>());
		}

		playersByType.get(newPlayer.type).add(newPlayer);

		addToRankings(newPlayer, positionToAdd);
	}

	private static void addToRankings(Player newPlayer, int positionToAdd) {
		if (positionToAdd == rankings.size()) {
			rankings.add(newPlayer);
		} else {
			rankings.add(positionToAdd, newPlayer);
		}

		output.append("Added player ").append(newPlayer.name).append(" to position ").append(positionToAdd + 1)
				.append("\n");

	}
}