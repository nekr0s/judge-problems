import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		int[] buildings = new int[n];
		for (int i = 0; i < n; i++) {
			buildings[i] = in.nextInt();
		}

		int[] maxJumps = new int[buildings.length];

		Stack<Integer> peeksIndices = new Stack<>();
		for (int i = buildings.length - 1; i >= 0; i--) {
			while (!peeksIndices.isEmpty() && buildings[peeksIndices.peek()] <= buildings[i]) {
				int peekIndex = peeksIndices.pop();
				maxJumps[peekIndex] = peeksIndices.size();
			}

			peeksIndices.push(i);
		}

		while (!peeksIndices.isEmpty()) {
			int peekIndex = peeksIndices.pop();
			maxJumps[peekIndex] = peeksIndices.size();
		}

		int max = Arrays.stream(maxJumps).max().getAsInt();
		System.out.println(max);

		for (int i = 0; i < maxJumps.length; i++) {
			if (i == maxJumps.length - 1) {
				System.out.print(maxJumps[i]);
			} else {
				System.out.print(maxJumps[i] + " ");
			}

		}
	}
}